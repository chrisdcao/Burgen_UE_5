// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ProcMeshActor.h"
#include "ApartmentSpecification.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "ThreadedWorker.h"
#include "atomic"
#include "HouseBuilder.generated.h"

#define DEFAULT_RAND_MODE -1
#define UPDATE_RAND_MODE -2

class ThreadedWorker;

UCLASS()
class CITY_API AHouseBuilder : public AActor
{
    GENERATED_BODY()

    unsigned int maxThreads = 8;

    bool wantsToWork = false;
    bool isWorking = false;
    int currentIndex = 0;
    int meshesPerTick = 3;
    TArray<FMeshInfoData> roomDecorMeshesInfo;
    TArray<UTextRenderComponent*> texts;

public:
    int currRandMode = DEFAULT_RAND_MODE;

    UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
    UBoxComponent* boxfarcollision;

    UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
    UBoxComponent* boxclosecollision;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=generation)
    AProcMeshActor* procMeshActor;

    bool firstTime = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation, DisplayName="Auto Update?")
    bool bAutoUpdate = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation, DisplayName="Shell Only?")
    bool bShellOnly = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation)
    FHousePolygon housePoly;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation)
    float corrWidth = 300;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation)
    float floor_height = 400.0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation,
        DisplayName="Number of times regenerate house shape")
    int tryMakeInterestingNumberOfAttempts = 4;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = generation, meta = (AllowPrivateAccess = "true"))
    float maxChangeIntensity = 0.35;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation)
    bool bGenerateRoofs = true;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = performance, meta = (AllowPrivateAccess = "true"))
    GenerationMode generationMode;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=generation, DisplayName="Current Seed")
    int64 currSeed;

    UFUNCTION(CallInEditor, Category=generation, DisplayName="Build")
    void UpdateHouse();

    static std::atomic<unsigned int> housesWorking;

    // we use atomic for `workersWorking` because this variable is being accessed, written to and read from in multiple threads
    // (functionality is similar to normal built-in type variable <unsigned int>, but thread-safe)
    static std::atomic<unsigned int> workersWorking;

    // Sets default values for this actor's properties
    AHouseBuilder();
    virtual ~AHouseBuilder() override;

    // similar to unordered_map, use hashing
    // key: name of the decor, value: static mesh of the decor
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = meshes, meta = (AllowPrivateAccess = "true"))
    TMap<FString, UHierarchicalInstancedStaticMeshComponent*> map;


    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = ProcMesh)
    TSubclassOf<class AProcMeshActor> procMeshActorClass;


    UFUNCTION(BlueprintCallable, Category = "Generation")
    void init(FHousePolygon f_in, float floorHeight_in, int makeInterestingAttempts_in, bool generateRoofs_in,
              GenerationMode generationMode_in)
    {
        housePoly = f_in;
        floor_height = floorHeight_in;
        tryMakeInterestingNumberOfAttempts = makeInterestingAttempts_in;
        floor_height = floorHeight_in;
        bGenerateRoofs = generateRoofs_in;
        generationMode = generationMode_in;
        switch (generationMode)
        {
        case GenerationMode::complete: maxThreads = 1000;
            meshesPerTick = 1000;
            break;
        case GenerationMode::procedural_aggressive: maxThreads = 1000;
            meshesPerTick = 50;
            break;
        case GenerationMode::procedural_relaxed: maxThreads = 1;
            meshesPerTick = 5;
            break;
        }
    }

    UFUNCTION(BlueprintCallable, Category = "Generation")
    void setGenerationMode(GenerationMode generationMode_in)
    {
        generationMode = generationMode_in;
        switch (generationMode)
        {
        case GenerationMode::complete: maxThreads = 1000;
            meshesPerTick = 1000;
            break;
        case GenerationMode::procedural_aggressive: maxThreads = 1000;
            meshesPerTick = 50;
            break;
        case GenerationMode::procedural_relaxed: maxThreads = 1;
            meshesPerTick = 5;
            break;
        }
    }


    UFUNCTION(BlueprintCallable, Category = "Generation")
    FHouseInfo getHouseInfo(int randMode);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    void buildHouse(bool shellOnly);

    void addDecorMeshSimplePlot(FSimplePlot& nonHousePlot);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    void buildHouseFromInfo(FHouseInfo houseInfo);

    bool hasIntersection(FPolygon& poly1, FPolygon& poly2);

    static void makeInteresting(FHousePolygon& f, TArray<FSimplePlot>& toReturn, FPolygon& centerHole,
                                FRandomStream stream);

    static FPolygon getShaftHolePolygon(FHousePolygon f, FRandomStream stream, bool useCenter = false);

    ThreadedWorker* worker;

    bool workerWorking = false;
    bool workerWantsToWork = false;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    void addDecorMeshInHouse(int nextStop);

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

    UFUNCTION(BlueprintCallable)
    void housePlace(FHousePolygon housePolygon, float floorHeight, bool shellOnly, int makeInterestingAttempts,
                    bool generateRoofs, GenerationMode proceduralLoadIn);
};

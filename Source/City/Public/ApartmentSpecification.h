#pragma once

#include "CoreMinimal.h"
#include "RoomBuilder.h"


/**
 * In here and in ApartmentSpecification.cpp the specifications for the different apartments are defined. Expanding the number of apartments should be easy by just following the same structure, make sure you use the new specifications when generating interiors in HouseBuilder as well.
 */
class CITY_API ApartmentSpecification
{
public:
    ApartmentSpecification();
    virtual ~ApartmentSpecification();
    virtual RoomBlueprint getBlueprint(float areaScale) = 0;
    virtual FRoomInfo buildApartment(FRoomPolygon* f, int floor, float height,
                                     TMap<FString, UHierarchicalInstancedStaticMeshComponent*>& map,
                                     bool potentialBalcony, bool shellOnly, FRandomStream stream);

    virtual void intermediateInteractWithRooms(TArray<FRoomPolygon*>& roomPols, FRoomInfo& r,
                                               TMap<FString, UHierarchicalInstancedStaticMeshComponent*>& map,
                                               bool potentialBalcony)
    {
    };
    void placeEntranceMeshes(FRoomInfo& r, FRoomPolygon* r2);
    virtual float getWindowDensity(FRandomStream stream) = 0;
    virtual float getWindowWidth(FRandomStream stream) = 0;
    virtual float getWindowHeight(FRandomStream stream) = 0;
    virtual bool getWindowFrames() = 0;
    virtual float getMaxApartmentSize() = 0;
};

class CITY_API OfficeSpecification : public ApartmentSpecification
{
public:
    virtual RoomBlueprint getBlueprint(float areaScale) override;
    virtual float getWindowDensity(FRandomStream stream) override { return 1; }
    virtual float getWindowWidth(FRandomStream stream) override { return stream.FRandRange(200, 400); }
    virtual float getWindowHeight(FRandomStream stream) override { return stream.FRandRange(200, 300); }
    virtual bool getWindowFrames() override { return false; }
    virtual float getMaxApartmentSize() override { return 600; }
};


class CITY_API LivingSpecification : public ApartmentSpecification
{
public:
    virtual RoomBlueprint getBlueprint(float areaScale) override;
    virtual void intermediateInteractWithRooms(TArray<FRoomPolygon*>& roomPols, FRoomInfo& r,
                                               TMap<FString, UHierarchicalInstancedStaticMeshComponent*>& map,
                                               bool potentialBalcony) override;
    virtual float getWindowDensity(FRandomStream stream) override { return 0.003; }
    virtual float getWindowWidth(FRandomStream stream) override { return 200.0f; }
    virtual float getWindowHeight(FRandomStream stream) override { return 200.0f; }
    virtual bool getWindowFrames() override { return true; }
    virtual float getMaxApartmentSize() override { return 400; }
};

class CITY_API StoreSpecification : public ApartmentSpecification
{
public:
    virtual RoomBlueprint getBlueprint(float areaScale) override;
    virtual void intermediateInteractWithRooms(TArray<FRoomPolygon*>& roomPols, FRoomInfo& r,
                                               TMap<FString, UHierarchicalInstancedStaticMeshComponent*>& map,
                                               bool potentialBalcony) override;
    virtual float getWindowDensity(FRandomStream stream) override { return 1; }
    virtual float getWindowWidth(FRandomStream stream) override { return stream.FRandRange(200, 400); }
    virtual float getWindowHeight(FRandomStream stream) override { return stream.FRandRange(200, 300); }
    virtual bool getWindowFrames() override { return true; }
    virtual float getMaxApartmentSize() override { return 5000; }
};

class CITY_API RestaurantSpecification : public ApartmentSpecification
{
public:
    virtual RoomBlueprint getBlueprint(float areaScale) override;
    virtual void intermediateInteractWithRooms(TArray<FRoomPolygon*>& roomPols, FRoomInfo& r,
                                               TMap<FString, UHierarchicalInstancedStaticMeshComponent*>& map,
                                               bool potentialBalcony) override;
    virtual float getWindowDensity(FRandomStream stream) override { return 1; }
    virtual float getWindowWidth(FRandomStream stream) override { return stream.FRandRange(200, 400); }
    virtual float getWindowHeight(FRandomStream stream) override { return stream.FRandRange(200, 300); }
    virtual bool getWindowFrames() override { return true; }
    virtual float getMaxApartmentSize() override { return 5000; }
};

#pragma once

#include "GameFramework/Actor.h"
#include "BaseLibrary.h"
#include "ProceduralMeshComponent.h"
#include "ProcMeshActor.generated.h"


UCLASS()
class CITY_API AProcMeshActor : public AActor
{
    GENERATED_BODY()

public:
    static unsigned int workersWorking;
    GenerationMode generationMode;
    unsigned int maxThreads = 10000;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=appearance, DisplayName="Shell Only?")
    bool isShellOnly;
    FVector cachedVectorOffset;
    TArray<FMaterialPolygon> cachedMaterialPols;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=appearance)
    TArray<AActor*> AssociatedActors;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=appearance)
    bool bAutoUpdate = true;

    // Sets default values for this actor's properties
    AProcMeshActor();
    virtual ~AProcMeshActor() override;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* exteriorMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* sndExteriorMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* windowMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* windowFrameMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* occlusionWindowMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* interiorMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* floorMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* roofMat = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* greenMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* concreteMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* roadMiddleMat = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    UMaterial* asphaltMat = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = appearance, meta = (AllowPrivateAccess = "true"))
    float texScaleMultiplier = 1.0f;

    bool wantsToWork = false;
    bool isWorking = false;
    int currentlyWorkingArray = 0;

    UPROPERTY()
    USceneComponent* SceneRootComponent = nullptr;

    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* exteriorMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* interiorMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* windowMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* occlusionWindowMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* windowFrameMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* sndExteriorMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* floorMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* roofMesh = nullptr;

    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* greenMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* concreteMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* roadMiddleMesh = nullptr;
    UPROPERTY(VisibleAnywhere, Category = Meshes)
    UProceduralMeshComponent* asphaltMesh = nullptr;

    TArray<UProceduralMeshComponent*> components;
    TArray<UMaterialInterface*> materials;
    TArray<TArray<FPolygon>> polygons;

    int currIndex = 1;


    // FUNCTIONS
private:
    bool buildPolygons(TArray<FPolygon>& pols, FVector offset, UProceduralMeshComponent* mesh, UMaterialInterface* mat);

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

    virtual void PostActorCreated() override;

    UFUNCTION(BlueprintImplementableEvent)
    void ConstructInitialData();

    UFUNCTION(CallInEditor, Category=appearance)
    void Regenerate();

public:
    UFUNCTION(BlueprintCallable, Category = "Generation")
    bool buildPolygonsWithMaterial(TArray<FMaterialPolygon> pols, FVector offset);

    bool clearMeshes(bool fullReplacement);

    UFUNCTION(BlueprintCallable, Category = "Settings")
    void init(GenerationMode generationMode_in)
    {
        generationMode = generationMode_in;
        switch (generationMode)
        {
        case GenerationMode::complete: maxThreads = 1000;
            break;
        case GenerationMode::procedural_aggressive: maxThreads = 1000;
            break;
        case GenerationMode::procedural_relaxed: maxThreads = 1;
            break;
        }
    }
};

// Fill out your copyright notice in the Description page of Project Settings.
// Plot is an intangible actor (cannot be seen in scene) that stands for the whole build-able area.
// This build-able could either be houses, decorations (lamp, bush, grass,etc..) or something, but all are based on info of the => within the given plot
#pragma once

#include "GameFramework/Actor.h"
#include "HouseBuilder.h"
//#include "BaseLibrary.h"
#include "PlotBuilder.generated.h"

USTRUCT(BlueprintType)
struct FSidewalkInfo
{
    GENERATED_USTRUCT_BODY()
    ;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FMeshInfoData> meshes;
};


UCLASS()
class CITY_API APlotBuilder : public AActor
{
    GENERATED_BODY()


    UPROPERTY()
    TArray<FSimplePlot> cachedSimplePlots;

    UPROPERTY()
    FSidewalkInfo cachedSideWalkInfo;

    UPROPERTY(BlueprintReadWrite, Category="Generation", meta = (AllowPrivateAccess = "true"))
    TMap<FString, UHierarchicalInstancedStaticMeshComponent*> instancedMap;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Generation", meta = (AllowPrivateAccess = "true"))
    bool bAutoUpdate = true;

public:
    // Sets default values for this actor's properties
    APlotBuilder();

    // cd.khanh begin insert 14/07/22
    // upper bound for randomizing max house area
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generation", DisplayName="Max House Size Upper Bound")
    float maxHouseArea_upperBound = 6000.0f;

    // lower bound for randomizing max house area
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generation", DisplayName="Max House Size Lower Bound")
    float maxHouseArea_lowerBound = 3000.0f;

    // min area that a house can be
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generation")
    float minHouseArea = 1200.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Generation")
    int similarHouses_upperBound = 6000.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Generation")
    int similarHouses_lowerBound = 3500.f;
    // cd.khanh end insert 14/07/22

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = meshes, meta = (AllowPrivateAccess = "true"))
    float noiseHeightInfluence = 0.0;

    UFUNCTION(BlueprintCallable, Category = "Generation")
    static TArray<FMetaPolygon> sanityCheck(TArray<FMetaPolygon> plotsToAdd, TArray<FPolygon> others);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    FPlotInfo generateHousePolygons(FPlotPolygon plotPolygon, int minFloors, int maxFloors);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    static FPolygon generateSidewalkPolygon(FPlotPolygon p, float offsetSize);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    TArray<FMaterialPolygon> getSideWalkPolygons(FPlotPolygon p, float width);


    UFUNCTION(BlueprintCallable, Category = "Generation")
    static FSidewalkInfo getSideWalkInfo(FPolygon& sidewalk);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    TArray<FMaterialPolygon> getSimplePlotPolygonsPB(const TArray<FSimplePlot>& plots);

    UFUNCTION(BlueprintCallable, Category = "Generation")
    static FCityDecoration getCityDecoration(TArray<FMetaPolygon> plots, TArray<FPolygon> roads);

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    //virtual void BeginDestroy() override;

    UFUNCTION(BlueprintImplementableEvent)
    void ConstructInitialData();

    virtual void PostActorCreated() override;

    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintCallable)
    virtual void Decorate(TArray<FMeshInfoData> meshesInfo);

    UFUNCTION(BlueprintCallable)
    virtual void DecorateSimplePlots(TArray<FSimplePlot> simplePlots);

    UFUNCTION(BlueprintCallable)
    virtual void DecorateSideWalk(FPolygon polygon);

    UFUNCTION(CallInEditor, Category="Generation")
    void Regenerate();
};
